import java.awt.Component;

import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;

public class ProgressWorker extends SwingWorker<Object, Object> {
	private final int MAX = 10_000_000;
	private Component parentComponent;
	
	ProgressWorker(Component _parentComponent) {
		parentComponent = _parentComponent;
	}
	
	@Override
	protected Object doInBackground() throws Exception {
		ProgressMonitor progressMonitor 
		= new ProgressMonitor(parentComponent, "progress", "now...", 0, 10_000_000);
		for (int i=0; i < MAX+1; i++) {
			for (int j = 0; j < 10; j++)
			progressMonitor.setProgress(i);
			if (i%25 == 0) {
				progressMonitor.setNote(Integer.toString(i));
			}
		}
		
		return null;
	}
}
