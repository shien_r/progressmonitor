import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonActionListener implements ActionListener {
	private Component parentComponent;
	
	ButtonActionListener(Component _parentComponent) {
		parentComponent = _parentComponent;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		ProgressWorker progressWorker = new ProgressWorker(parentComponent);
		progressWorker.execute();
		
	}

}
