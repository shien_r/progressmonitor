import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class ProgressMoniter extends JFrame implements Runnable{

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new ProgressMoniter());
	}
	
	@Override
	public void run() {
		this.setSize(200, 200);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("Progress Bar");
		this.setLayout(new GridLayout(1,1));
		JButton jButton = new JButton("press start");
		jButton.addActionListener(new ButtonActionListener(this));
		this.add(jButton);
		this.setVisible(true);
	}
}
